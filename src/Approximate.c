/**
 * Symbolic
 *
 * @file
 * @author  David Llewellyn-Jones <david@flypig.co.uk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * The MIT License
 * See symbolic.h, COPYING file or website for licence
 *
 * @section DESCRIPTION
 *
 * Library for the construction of nested symbolic propositions
 * The Flying Pig!
 * Started 5/8/2003
 * http://www.flypig.co.uk?to=symbolic
 *
 * The Approximate functions support conversion of a symbolic expression.
 * to a decimal with double precision.
 *
 */

//////////////////////////////////////////////////////////////////
// Includes

#include "symbolic.h"
#include "symbolic_private.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <math.h>
#include <float.h>

//////////////////////////////////////////////////////////////////
// Defines

//////////////////////////////////////////////////////////////////
// Structures

//////////////////////////////////////////////////////////////////
// Global variables

//////////////////////////////////////////////////////////////////
// Function prototypes

//////////////////////////////////////////////////////////////////
// Main application

/**
 * Recursively approximate an operation if possible.
 *
 * @param psOp the operation to evaluate recursively.
 * @return the approximated value.
 */
double ApproximateOperation (Operation * psOp) {
	double fReturn = NAN;
	double fVar1;
	double fVar2;

	// Check each of the structures
	if (psOp) {
		switch (psOp->eOpType) {
			case OPTYPE_INTEGER:
				// Integers approximate to themselves
				fReturn = (double)psOp->Vars.nInteger;
				break;
			case OPTYPE_TRUTHVALUE:
				// Truth values (T, F) evaluate to 1 and 0
				fReturn = (double)psOp->Vars.boTruth;
				break;
			case OPTYPE_VARIABLE:
				// Variables evaluate to the value stored in them
				if (psOp->Vars.psVar->psValue) {
					fReturn = GetVariable (psOp->Vars.psVar->psValue);
				}
				break;
			case OPTYPE_UNARY:
				// Unary operations evaluate to the operation applied to their evaluated parameter
				// Evaluate the parameter first
				fVar1 = ApproximateOperation (psOp->Vars.psUnary->psVar1);
				switch (psOp->Vars.psUnary->eOpType) {
					case OPUNARY_NEGATIVE:
						// The negative of the parameter
						fReturn = - fVar1;
						break;
					case OPUNARY_NOT:
						// The negation of the parameter
						fReturn = 1.0 - fVar1;
						break;
					case OPUNARY_COSEC:
						// Cosec (paramter)
						fReturn = 1.0 / sin (fVar1);
						break;
					case OPUNARY_SINH:
						// Hyperbolic sine (parameter)
						fReturn = sinh (fVar1);
						break;
					case OPUNARY_COSH:
						// Hyperbolic cosine (parameter)
						fReturn = cosh (fVar1);
						break;
					case OPUNARY_TANH:
						// Hyperbolic tan (parameter)
						fReturn = sinh (fVar1);
						break;
					case OPUNARY_COS:
						// Cosine (parameter)
						fReturn = cos (fVar1);
						break;
					case OPUNARY_SIN:
						// Sine (parameter)
						fReturn = sin (fVar1);
						break;
					case OPUNARY_TAN:
						// Tan (parameter)
						fReturn = tan (fVar1);
						break;
					case OPUNARY_SEC:
						// Sec (parameter)
						fReturn = 1.0 / cos (fVar1);
						break;
					case OPUNARY_COT:
						// Cotan (parameter)
						fReturn = 1.0 / tan (fVar1);
						break;
					case OPUNARY_ARCSIN:
						// Arc sine (parameter)
						fReturn = asin (fVar1);
						break;
					case OPUNARY_ARCCOS:
						// Arc cosine (parameter)
						fReturn = acos (fVar1);
						break;
					case OPUNARY_ARCTAN:
						// Arc tan (parameter)
						fReturn = atan (fVar1);
						break;
					case OPUNARY_LN:
						// Natural logarithm (parameter)
						fReturn = log (fVar1);
					case OPUNARY_EXP:
						// e^(parameter)
						fReturn = exp (fVar1);
						break;
					case OPUNARY_ABS:
						// Absolute (positive) value (parameter)
						fReturn = fabs (fVar1);
						break;
					default:
						// Do nothing (return NAN)
						break;
				}
				break;
			case OPTYPE_UNARYUSER:
				fReturn = UserApproximateDefault (psOp);
				break;
			case OPTYPE_BINARY:
				// Binary operations are applied to the result of approximating their operands (recursive operation)
				// We evaluate the operands first
				fVar1 = ApproximateOperation (psOp->Vars.psBinary->psVar1);
				fVar2 = ApproximateOperation (psOp->Vars.psBinary->psVar2);
				switch (psOp->Vars.psBinary->eOpType) {
					case OPBINARY_LAND:
						// Logical AND
						fReturn = (int)fVar1 && (int)fVar2;
						break;
					case OPBINARY_LOR:
						// Logical OR
						fReturn = (int)fVar1 || (int)fVar2;
						break;
					case OPBINARY_LIMP:
						// Logical implication
						break;
					case OPBINARY_POW:
						// a^b
						fReturn = pow (fVar1, fVar2);
						break;
					case OPBINARY_ADD:
						// a + b
						fReturn = fVar1 + fVar2;
						break;
					case OPBINARY_SUB:
						// a - b
						fReturn = fVar1 - fVar2;
						break;
					case OPBINARY_MUL:
						// a * b
						fReturn = fVar1 * fVar2;
						break;
					case OPBINARY_DIV:
						// a DIV b
						// TODO: This looks wrong though. Check.
						fReturn = (int)fVar1 / (int)fVar2;
						break;
					case OPBINARY_DIVIDE:
						// a / b
						fReturn = fVar1 / fVar2;
						break;
					case OPBINARY_MOD:
						// a MOD b (remainder)
						if (!(isnan (fVar1) || isnan (fVar2))) {
							fReturn = fmod (fVar1, fVar2);
						}
						break;
					case OPBINARY_OR:
						// Bitwise OR
						fReturn = (int)fVar1 | (int)fVar2;
						break;
					case OPBINARY_AND:
						// Bitwise AND
						fReturn = (int)fVar1 & (int)fVar2;
						break;
					case OPBINARY_BIC:
						// Bitwise bit clear
						fReturn = (int)fVar1 & !(int)fVar2;
						break;
					case OPBINARY_EOR:
						// Bitwise Exlusive OR
						fReturn = (int)fVar1 ^ (int)fVar2;
						break;
					case OPBINARY_ROR:
						// Rotate bits right
						// TODO: Implement this
						break;
					case OPBINARY_ROL:
						// Rotate bits left
						// TODO: Implement this
						break;
					case OPBINARY_EQ:
						// Equals comparison (a == b)
						if (!(isnan (fVar1) || isnan (fVar2))) {
							fReturn = (fVar1 == fVar2);
						}
						break;
					case OPBINARY_NE:
						// Not equals comparison (a != b(
						if (!(isnan (fVar1) || isnan (fVar2))) {
							fReturn = (fVar1 != fVar2);
						}
						break;
					case OPBINARY_LT:
						// Less than comparison (a < b)
						if (!(isnan (fVar1) || isnan (fVar2))) {
							fReturn = (fVar1 < fVar2);
						}
						break;
					case OPBINARY_LE:
						// Less than or equals to comparison (a <= b)
						if (!(isnan (fVar1) || isnan (fVar2))) {
							fReturn = (fVar1 <= fVar2);
						}
						break;
					case OPBINARY_GT:
						// Greater than comparison (a > b)
						if (!(isnan (fVar1) || isnan (fVar2))) {
							fReturn = (fVar1 > fVar2);
						}
						break;
					case OPBINARY_GE:
						// Greater than or equals to comparison (a >= b)
						if (!(isnan (fVar1) || isnan (fVar2))) {
							fReturn = (fVar1 >= fVar2);
						}
						break;
					case OPBINARY_IND:
						// Memory indirection
						// TODO: Figure out what to do about this
						break;
					default:
						break;
				}
				break;
			case OPTYPE_TERNARY:
				// Ternary operations haven't yet been implemented
				break;
			default:
				// Do nothing (returns NAN)
				break;
		}
	}
	
	// Return the result
	return fReturn;
}

