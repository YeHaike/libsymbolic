/**
 * Symbolic
 *
 * @file
 * @author  David Llewellyn-Jones <david@flypig.co.uk>
 * @version 1.0
 *
 * @section LICENSE
 *
 * The MIT License
 * See symbolic.h, COPYING file or website for licence
 *
 * @section DESCRIPTION
 *
 * Library for the construction of nested symbolic propositions.
 * The Flying Pig!
 * Started 5/8/2003
 * http://www.flypig.co.uk?to=symbolic
 *
 * Provides functions for the simplification of Operations. The
 * process recursively applies a collection of rule (tautologies)
 * until no further simplifications are possible. This is a simple
 * form of theorom proving, with the input and output statements
 * being mathematically equivalent.
 * 
 */

//////////////////////////////////////////////////////////////////
// Includes

#include "symbolic.h"
#include "symbolic_private.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <math.h>

//////////////////////////////////////////////////////////////////
// Defines

//////////////////////////////////////////////////////////////////
// Structures

//////////////////////////////////////////////////////////////////
// Global variables

//////////////////////////////////////////////////////////////////
// Function prototypes

int gcd (int nA, int nB);
Operation * SimplifyNEGATIVE (Operation * psOp, Operation * psVar1);
Operation * SimplifyNOT (Operation * psOp, Operation * psVar1);
Operation * SimplifyADD (Operation * psOp, Operation * psVar1, Operation * psVar2);
Operation * SimplifySUB (Operation * psOp, Operation * psVar1, Operation * psVar2);
Operation * SimplifyMUL (Operation * psOp, Operation * psVar1, Operation * psVar2);
Operation * SimplifyDIV (Operation * psOp, Operation * psVar1, Operation * psVar2);
Operation * SimplifyDIVIDE (Operation * psOp, Operation * psVar1, Operation * psVar2);
Operation * SimplifyPOW (Operation * psOp, Operation * psVar1, Operation * psVar2);

//////////////////////////////////////////////////////////////////
// Main application

/**
 * Recursively simplify an operation if possible.
 * Applies a variety of proof rules (tautologies) to the original operation.
 * The result will therefore be mathematically equivalent.
 * Because a new version may created and the original may be freed
 * it's good practice to overwrite the variable pointing to the
 * input structure with the return value.
 *
 * @param psOp the operation to simplify, which may be entirely freed.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifyOperation (Operation * psOp) {
	Operation * psReturn = NULL;
	OpUnary * psUna = NULL;
	OpBinary * psBin = NULL;
	OpTernary * psTer = NULL;
	Operation * psTemp = NULL;

	if (psOp) {
		switch (psOp->eOpType) {
			case OPTYPE_MEMORY:
				// No simplification possible
				psReturn = psOp;
				break;
			case OPTYPE_INTEGER:
				// No simplification possible
				psReturn = psOp;
				break;
			case OPTYPE_TRUTHVALUE:
				// No simplification possible
				psReturn = psOp;
				break;
			case OPTYPE_VARIABLE:
				// No simplification possible
				psReturn = psOp;
				break;
			case OPTYPE_UNARY:
				psReturn = psOp;
				psUna = psReturn->Vars.psUnary;
				// Simplify the parameter operation
				psUna->psVar1 = SimplifyOperation (psUna->psVar1);
				switch (psUna->eOpType) {
					case OPUNARY_NEGATIVE:
						// Simplify unary negatie
						psReturn = SimplifyNEGATIVE (psOp, psUna->psVar1);
						break;
					case OPUNARY_NOT:
						// Simplify unary Logical NOT
						psReturn = SimplifyNOT (psOp, psUna->psVar1);
						break;
					case OPUNARY_LN:
						// Simplify unary natural logarithm
						if ((psUna->psVar1->eOpType == OPTYPE_INTEGER) && (psUna->psVar1->Vars.nInteger == 1)) {
							psReturn = CreateInteger (0);
							FreeRecursive (psOp);
						}
						break;
					default:
						// Do nothing
						break;
				}
				break;
			case OPTYPE_UNARYUSER:
				psReturn = UserSimplifyDefault (psOp);
				break;
			case OPTYPE_BINARY:
				// Simplify a binary operation
				psBin = psOp->Vars.psBinary;
				// First simplify the parameters
				psBin->psVar1 = SimplifyOperation (psBin->psVar1);
				psBin->psVar2 = SimplifyOperation (psBin->psVar2);
				psReturn = psOp;
				switch (psBin->eOpType) {
					case OPBINARY_LAND:
						// Simplify a logical AND
						// If either is FALSE, reduce to FALSE
						if (((psBin->psVar1->eOpType == OPTYPE_TRUTHVALUE)
							&& (!psBin->psVar1->Vars.boTruth))
							|| ((psBin->psVar2->eOpType == OPTYPE_TRUTHVALUE)
							&& (!psBin->psVar2->Vars.boTruth))) {
							psReturn = CreateTruthValue (FALSE);
							FreeRecursive (psOp);
						}
						else {
							// If the first parameter is TRUE, reduce to the second parameter
							if (((psBin->psVar1->eOpType == OPTYPE_TRUTHVALUE)
								&& (psBin->psVar1->Vars.boTruth))
								|| (CompareOperations (psBin->psVar1, psBin->psVar2))) {
								psReturn = CopyRecursive (psBin->psVar2);
								FreeRecursive (psOp);
							}
							else {
								// If the second parameter is TRUE, reduce to the first parameter
								if ((psBin->psVar2->eOpType == OPTYPE_TRUTHVALUE)
									&& (psBin->psVar2->Vars.boTruth)) {
									psReturn = CopyRecursive (psBin->psVar1);
									FreeRecursive (psOp);
								}
							}
						}
						break;
					case OPBINARY_LOR:
						// Simplify a logical Or
						// If either paremter is TRUE, reduce to TRUE
						if (((psBin->psVar1->eOpType == OPTYPE_TRUTHVALUE)
							&& (psBin->psVar1->Vars.boTruth))
							|| ((psBin->psVar2->eOpType == OPTYPE_TRUTHVALUE)
							&& (psBin->psVar2->Vars.boTruth))) {
							psReturn = CreateTruthValue (TRUE);
							FreeRecursive (psOp);
						}
						else {
							// If both parameters are FALSE, reduce to FALSE
							if ((psBin->psVar1->eOpType == OPTYPE_TRUTHVALUE)
								&& (!psBin->psVar1->Vars.boTruth)
								&& (psBin->psVar2->eOpType == OPTYPE_TRUTHVALUE)
								&& (!psBin->psVar2->Vars.boTruth)) {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_LIMP:
						// Simplify logical implication
						// If the first parameter is FALSE or the second is TRUE, reduce to TRUE
						if ((((psBin->psVar1->eOpType == OPTYPE_TRUTHVALUE)
							&& (!psBin->psVar1->Vars.boTruth))
							|| ((psBin->psVar2->eOpType == OPTYPE_TRUTHVALUE)
							&& (psBin->psVar2->Vars.boTruth)))
							|| CompareOperations (psBin->psVar1, psBin->psVar2)) {
							psReturn = CreateTruthValue (TRUE);
							FreeRecursive (psOp);
						}
						else {
							/// If the first parameeter is TRUE, reduce to the second parameter
							if ((psBin->psVar1->eOpType == OPTYPE_TRUTHVALUE)
								&& (psBin->psVar1->Vars.boTruth)) {
								psReturn = CopyRecursive (psBin->psVar2);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_POW:
						// Simplify POW operation
						psReturn = SimplifyPOW (psOp, psBin->psVar1, psBin->psVar2);
						break;
					case OPBINARY_ADD:
						// Simplify addition
						psReturn = SimplifyADD (psOp, psBin->psVar1, psBin->psVar2);
						break;
					case OPBINARY_SUB:
						// Simplify subtraction
						psReturn = SimplifySUB (psOp, psBin->psVar1, psBin->psVar2);
						break;
					case OPBINARY_MUL:
						// Simplify multiplication
						psReturn = SimplifyMUL (psOp, psBin->psVar1, psBin->psVar2);
						break;
					case OPBINARY_DIV:
						// Simmplify DIV
						psReturn = SimplifyDIV (psOp, psBin->psVar1, psBin->psVar2);
						break;
					case OPBINARY_DIVIDE:
						// Simplify divisiono
						psReturn = SimplifyDIVIDE (psOp, psBin->psVar1, psBin->psVar2);
						break;
					case OPBINARY_MOD:
						// Simplify MOD
						// If both parameters are integers, perform the modulus
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							psReturn = CreateInteger (psBin->psVar1->Vars.nInteger
								% psBin->psVar2->Vars.nInteger);
							FreeRecursive (psOp);
						}
						break;
					case OPBINARY_OR:
						// Simplify bitwise OR
						// If both parameters are integers, perform the bitwise OR
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							psReturn = CreateInteger (psBin->psVar1->Vars.nInteger
								| psBin->psVar2->Vars.nInteger);
							FreeRecursive (psOp);
						}
						break;
					case OPBINARY_AND:
						// Simplify bitwise AND
						// If both parameters are integers, perform the bitwise AND
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							psReturn = CreateInteger (psBin->psVar1->Vars.nInteger
								& psBin->psVar2->Vars.nInteger);
							FreeRecursive (psOp);
						}
						break;
					case OPBINARY_BIC:
						// Simplify bitwise BIC
						// If both parameters are integers, perform the bitwise bitclear
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							psReturn = CreateInteger (psBin->psVar1->Vars.nInteger
								& !psBin->psVar2->Vars.nInteger);
							FreeRecursive (psOp);
						}
						break;
					case OPBINARY_EOR:
						// Simplify bitwise EOR
						// If both parameters are integers, perform the bitwise exclusive OR
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							psReturn = CreateInteger (psBin->psVar1->Vars.nInteger
								^ psBin->psVar2->Vars.nInteger);
							FreeRecursive (psOp);
						}
						break;
					case OPBINARY_ROR:
						// Simplify bitwise ROR
						// If both parameters are integers, rotate the bits right
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							psReturn = CreateInteger (psBin->psVar1->Vars.nInteger
								>> psBin->psVar2->Vars.nInteger);
							FreeRecursive (psOp);
						}
						break;
					case OPBINARY_ROL:
						// Simplify bitwise ROL
						// If both parameters are integers, rotate the bits left
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							psReturn = CreateInteger (psBin->psVar1->Vars.nInteger
								<< psBin->psVar2->Vars.nInteger);
							FreeRecursive (psOp);
						}
						break;
					case OPBINARY_EQ:
						// Simplify equivalence comparison
						// If both parameters are integers, perform the comparison
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							// Perform the comparison
							if (psBin->psVar1->Vars.nInteger == psBin->psVar2->Vars.nInteger) {
								psReturn = CreateTruthValue (TRUE);
								FreeRecursive (psOp);
							}
							else {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						else {
							// They're not integers, so check whether they happen to be identical operations
							if (CompareOperations (psBin->psVar1, psBin->psVar2)) {
								psReturn = CreateTruthValue (TRUE);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_NE:
						// Simplify non-equivalence comparison
						// If both parameters are integers, perform the comparison
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							// Perform the comparison
							if (psBin->psVar1->Vars.nInteger
								!= psBin->psVar2->Vars.nInteger) {
								psReturn = CreateTruthValue (TRUE);
								FreeRecursive (psOp);
							}
							else {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						else {
							// They're not integers, so check whether they happen to be identical operations
							if (CompareOperations (psBin->psVar1, psBin->psVar2)) {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_LT:
						// Simplify less than comparison
						// If both parameters are integers, perform the comparison
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							// Perform the comparison
							if (psBin->psVar1->Vars.nInteger
								< psBin->psVar2->Vars.nInteger) {
								psReturn = CreateTruthValue (TRUE);
								FreeRecursive (psOp);
							}
							else {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_LE:
						// Simplify less than or equal to comparison
						// If both parameters are integers, perform the comparison
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							// Perform the comparison
							if (psBin->psVar1->Vars.nInteger
								<= psBin->psVar2->Vars.nInteger) {
								psReturn = CreateTruthValue (TRUE);
								FreeRecursive (psOp);
							}
							else {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_GT:
						// Simplify greater than comparison
						// If both parameters are integers, perform the comparison
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							// Perform the comparison
							if (psBin->psVar1->Vars.nInteger
								> psBin->psVar2->Vars.nInteger) {
								psReturn = CreateTruthValue (TRUE);
								FreeRecursive (psOp);
							}
							else {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_GE:
						// Simplify greater than or equal to comparison
						// If both parameters are integers, perform the comparison
						if ((psBin->psVar1->eOpType == OPTYPE_INTEGER)
							&& (psBin->psVar2->eOpType == OPTYPE_INTEGER)) {
							// Perform the comparison
							if (psBin->psVar1->Vars.nInteger
								>= psBin->psVar2->Vars.nInteger) {
								psReturn = CreateTruthValue (TRUE);
								FreeRecursive (psOp);
							}
							else {
								psReturn = CreateTruthValue (FALSE);
								FreeRecursive (psOp);
							}
						}
						break;
					case OPBINARY_IND:
						// Search for a 'set' reference in the memory
						psTemp = psBin->psVar1;
						// Unravel the indirection as many times as needed
						while ((psTemp->eOpType == OPTYPE_TERNARY)
							&& (psTemp->Vars.psTernary->eOpType == OPTERNARY_SET)
							&& (!CompareOperations (psBin->psVar2,
							psTemp->Vars.psTernary->psVar2))
							&& ((psTemp->Vars.psTernary->psVar2->eOpType != OPTYPE_BINARY)
							|| ((psTemp->Vars.psTernary->psVar2->eOpType == OPTYPE_BINARY)
							&& (psTemp->Vars.psTernary->psVar2->Vars.psBinary->eOpType
							!= OPBINARY_IND)))) {
							psTemp = psTemp->Vars.psTernary->psVar1;
						}
						if ((psTemp->eOpType == OPTYPE_TERNARY)
							&& (psTemp->Vars.psTernary->eOpType == OPTERNARY_SET)
							&& (CompareOperations (psBin->psVar2,
							psTemp->Vars.psTernary->psVar2))
							&& ((psTemp->Vars.psTernary->psVar2->eOpType != OPTYPE_BINARY)
							|| ((psTemp->Vars.psTernary->psVar2->eOpType == OPTYPE_BINARY)
							&& (psTemp->Vars.psTernary->psVar2->Vars.psBinary->eOpType
							!= OPBINARY_IND)))) {
							psReturn = CopyRecursive (psTemp->Vars.psTernary->psVar3);
							FreeRecursive (psOp);
						}
//						psTemp = psBin->psVar1;
//						while ((psTemp->eOpType == OPTYPE_TERNARY)
//							&& (psTemp->Vars.psTernary->eOpType == OPTERNARY_SET)
//							&& (psTemp->Vars.psTernary->psVar2->eOpType == OPTYPE_BINARY)
//							&& (psTemp->Vars.psTernary->psVar2->Vars.psBinary->eOpType
//							== OPBINARY_IND)
//							&& (!CompareOperations (psBin->psVar2,
//							psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2))
//							&& ((psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2->
//							eOpType != OPTYPE_BINARY)
//							|| ((psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2->
//							eOpType == OPTYPE_BINARY)
//							&& (psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2->
//							Vars.psBinary->eOpType != OPBINARY_IND))))
//						{
//							psTemp = psTemp->Vars.psTernary->psVar1;
//						}
//						if ((psTemp->eOpType == OPTYPE_TERNARY)
//							&& (psTemp->Vars.psTernary->eOpType == OPTERNARY_SET)
//							&& (psTemp->Vars.psTernary->psVar2->eOpType == OPTYPE_BINARY)
//							&& (psTemp->Vars.psTernary->psVar2->Vars.psBinary->eOpType
//							== OPBINARY_IND)
//							&& (CompareOperations (psBin->psVar2,
//							psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2))
//							&& ((psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2->
//							eOpType != OPTYPE_BINARY)
//							|| ((psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2->
//							eOpType == OPTYPE_BINARY)
//							&& (psTemp->Vars.psTernary->psVar2->Vars.psBinary->psVar2->
//							Vars.psBinary->eOpType != OPBINARY_IND))))
//						{
//							psReturn = CopyRecursive (psTemp->Vars.psTernary->psVar3);
//							FreeRecursive (psOp);
//						}
						break;
					default:
						break;
				}
				break;
			case OPTYPE_TERNARY:
				// Simplify a ternary operation
				psTer = psOp->Vars.psTernary;
				psTer->psVar1 = SimplifyOperation (psTer->psVar1);

				psTer->psVar2 = SimplifyOperation (psTer->psVar2);
//				if ((psTer->eOpType == OPTERNARY_SET)
//					&& (psTer->psVar2->eOpType == OPTYPE_BINARY)
//					&& (psTer->psVar2->Vars.psBinary->eOpType == OPBINARY_IND))
//				{
//					// Skip the ind operation,
//					// since we can't have a set without an ind!
//					psTer->psVar2->Vars.psBinary->psVar1 =
//						SimplifyOperation (psTer->psVar2->Vars.psBinary->psVar1);
//					psTer->psVar2->Vars.psBinary->psVar2 =
//						SimplifyOperation (psTer->psVar2->Vars.psBinary->psVar2);
//				}
//				else
//				{
//					psTer->psVar2 = SimplifyOperation (psTer->psVar2);
//				}
				psTer->psVar3 = SimplifyOperation (psTer->psVar3);
				psReturn = psOp;
				switch (psTer->eOpType) {
					case OPTERNARY_SET:
						if ((psTer->psVar1->eOpType == OPTYPE_TERNARY)
							&& (CompareOperations (psTer->psVar2,
							psTer->psVar1->Vars.psTernary->psVar2))) {
							psReturn = CreateTernary (OPTERNARY_SET,
								CopyRecursive (psTer->psVar1->Vars.psTernary->psVar1),
								CopyRecursive (psTer->psVar2),
								CopyRecursive (psTer->psVar3));
							FreeRecursive (psOp);
						}
						break;
					default:
						break;
				}
				break;
			default:
				psReturn = psOp;
				printf("Invalid operation type\n");
				break;
		}
	}
	return psReturn;
}

/**
 * Return the Greatest Common Denominator of nA and nB.
 *
 * @param nA input integer.
 * @param nB input integer.
 * @return the greatest common denominator of nA and nB.
 *
 */
int gcd (int nA, int nB) {
	return (nB == 0 ? nA : gcd (nB, nA % nB));
}

/**
 * Recursively simplify a NEGATIVE operation if possible.
 * This is for internal use.
 *
 * @param psOp the operation to be simplified.
 * @param psVar1 the parameter of the unary operation.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifyNEGATIVE (Operation * psOp, Operation * psVar1) {
	Operation * psReturn = psOp;
	OpBinary * psBin = NULL;

	if (psVar1->eOpType == OPTYPE_INTEGER) {
		psReturn = CreateInteger (- psVar1->Vars.nInteger);
		FreeRecursive (psOp);
	}
	else if ((psVar1->eOpType == OPTYPE_UNARY)
		&& (psVar1->Vars.psUnary->eOpType == OPUNARY_NEGATIVE))	{
		// Remove pairs of 'negatives'
		psReturn = CopyRecursive (psVar1->Vars.psUnary->psVar1);
		FreeRecursive (psOp);
	}
	else if (psVar1->eOpType == OPTYPE_BINARY) {
		psBin = psVar1->Vars.psBinary;
		switch (psBin->eOpType) {
			case OPBINARY_SUB:
				// Switch round terms
				psReturn = CreateBinary (OPBINARY_SUB, CopyRecursive (psBin->psVar2),
					CopyRecursive (psBin->psVar1));
				FreeRecursive (psOp);
				break;
			default:
				// Do nothing
				break;
		}
	}

	return psReturn;
}

/**
 * Recursively simplify a NOT operation if possible.
 * This is for internal use.
 *
 * @param psOp the operation to be simplified.
 * @param psVar1 the parameter of the unary operation.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifyNOT (Operation * psOp, Operation * psVar1) {
	Operation * psReturn = psOp;
	OpBinary * psBin = NULL;

	switch (psVar1->eOpType) {
		case OPTYPE_UNARY:
			if (psVar1->Vars.psUnary->eOpType == OPUNARY_NOT) {
				// Remove pairs of 'nots'
				psReturn = CopyRecursive (psVar1->Vars.psUnary->psVar1);
				FreeRecursive (psOp);
			}
			break;
		case OPTYPE_TRUTHVALUE:
			// Negate the truth value
			psReturn = CreateTruthValue (!psVar1->Vars.boTruth);
			FreeRecursive (psOp);
			break;
		case OPTYPE_BINARY:
			psBin = psVar1->Vars.psBinary;
			switch (psBin->eOpType) {
				case OPBINARY_EQ:
					// Change to NE
					psReturn = CreateBinary (OPBINARY_NE, psBin->psVar1, psBin->psVar2);
					FreeRecursive (psOp);
					break;
				case OPBINARY_NE:
					// Change to EQ
					psReturn = CreateBinary (OPBINARY_EQ, psBin->psVar1, psBin->psVar2);
					FreeRecursive (psOp);
					break;
				case OPBINARY_LT:
					// Change to GE
					psReturn = CreateBinary (OPBINARY_GE, psBin->psVar1, psBin->psVar2);
					FreeRecursive (psOp);
					break;
				case OPBINARY_LE:
					// Change to GT
					psReturn = CreateBinary (OPBINARY_GT, psBin->psVar1, psBin->psVar2);
					FreeRecursive (psOp);
					break;
				case OPBINARY_GT:
					// Change to LE
					psReturn = CreateBinary (OPBINARY_LE, psBin->psVar1,
						psBin->psVar2);
					FreeRecursive (psOp);
					break;
				case OPBINARY_GE:
					// Change to LT
					psReturn = CreateBinary (OPBINARY_LT, psBin->psVar1, psBin->psVar2);
					FreeRecursive (psOp);
					break;
				default:
					// Do nothing
					break;
			}
			break;
		default:
			// Do nothing
			break;
	}

	return psReturn;
}

/**
 * Recursively simplify an ADD operation if possible.
 * This is for internal use.
 *
 * @param psOp the operation to be simplified.
 * @param psVar1 the first parameter of the binary operation.
 * @param psVar2 the second parameter of the binary operation.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifyADD (Operation * psOp, Operation * psVar1, Operation * psVar2) {
	Operation * psReturn = psOp;

	if ((psVar1->eOpType == OPTYPE_INTEGER)
		&& (psVar2->eOpType == OPTYPE_INTEGER)) {
		// Add two integers
		psReturn = CreateInteger (psVar1->Vars.nInteger
			+ psVar2->Vars.nInteger);
		FreeRecursive (psOp);
	}
	else if ((psVar1->eOpType == OPTYPE_INTEGER)
		&& (psVar1->Vars.nInteger == 0)) {
		// Remove addition with 0
		psReturn = CopyRecursive (psVar2);
		FreeRecursive (psOp);
	}
	else if ((psVar2->eOpType == OPTYPE_INTEGER)
		// Remove addition with 0
		&& (psVar2->Vars.nInteger == 0)) {
		psReturn = CopyRecursive (psVar1);
		FreeRecursive (psOp);
	}
	else if ((psVar1->eOpType == OPTYPE_BINARY)
		&& (psVar2->eOpType == OPTYPE_INTEGER)) {
		// Additioon of binary and integer
		if ((psVar1->Vars.psBinary->eOpType == OPBINARY_ADD)
			&& (psVar1->Vars.psBinary->psVar2->eOpType == OPTYPE_INTEGER)) {
			// Combine addition and addition
			psReturn = CreateBinary (OPBINARY_ADD,
				CopyRecursive (psVar1->Vars.psBinary->psVar1),
				CreateInteger (psVar1->Vars.psBinary->psVar2->
				Vars.nInteger + psVar2->Vars.nInteger));
			FreeRecursive (psOp);
		}
		else if ((psVar1->Vars.psBinary->eOpType == OPBINARY_SUB)
			&& (psVar1->Vars.psBinary->psVar2->eOpType == OPTYPE_INTEGER)) {
			// Combine addition and subtraction
			if (psVar1->Vars.psBinary->psVar2->Vars.nInteger
				< psVar2->Vars.nInteger) {
				psReturn = CreateBinary (OPBINARY_ADD,
					CopyRecursive (psVar1->Vars.psBinary->psVar1),
					CreateInteger (psVar2->Vars.nInteger -
					psVar1->Vars.psBinary->psVar2->Vars.nInteger));
			}
			else {
				psReturn = CreateBinary (OPBINARY_SUB,
					CopyRecursive (psVar1->Vars.psBinary->psVar1),
					CreateInteger (psVar1->Vars.psBinary->psVar2->
					Vars.nInteger - psVar2->Vars.nInteger));
			}
			FreeRecursive (psOp);
		}
		else if ((psVar1->Vars.psBinary->eOpType == OPBINARY_DIVIDE)
			&& (psVar1->Vars.psBinary->psVar1->eOpType == OPTYPE_INTEGER)
			&& (psVar1->Vars.psBinary->psVar2->eOpType == OPTYPE_INTEGER)) {
			// Useful for simplifying continues fractions
			psReturn = CreateBinary (OPBINARY_DIVIDE, 
				CreateInteger (psVar1->Vars.psBinary->psVar1->Vars.nInteger
				+ (psVar1->Vars.psBinary->psVar2->Vars.nInteger * psVar2->Vars.nInteger)),
				CopyRecursive (psVar1->Vars.psBinary->psVar2));
			FreeRecursive (psOp);
		}
	}
	else if ((psVar1->eOpType == OPTYPE_INTEGER)
		&& (psVar2->eOpType == OPTYPE_BINARY)) {
		// Combine integer with binary
		if ((psVar2->Vars.psBinary->eOpType == OPBINARY_ADD)
			&& (psVar2->Vars.psBinary->psVar2->eOpType == OPTYPE_INTEGER)) {
			// Addition with addition
			psReturn = CreateBinary (OPBINARY_ADD,
				CopyRecursive (psVar2->Vars.psBinary->psVar1),
				CreateInteger (psVar2->Vars.psBinary->psVar2->
				Vars.nInteger + psVar1->Vars.nInteger));
			FreeRecursive (psOp);
		}
		else if ((psVar2->Vars.psBinary->eOpType == OPBINARY_SUB)
			&& (psVar2->Vars.psBinary->psVar2->eOpType == OPTYPE_INTEGER)) {
			// Subtraction with addition
			if (psVar2->Vars.psBinary->psVar2->Vars.nInteger
				< psVar1->Vars.nInteger) {
				psReturn = CreateBinary (OPBINARY_ADD,
					CopyRecursive (psVar2->Vars.psBinary->psVar1),
					CreateInteger (psVar1->Vars.nInteger -
					psVar2->Vars.psBinary->psVar2->Vars.nInteger));
			}
			else {
				psReturn = CreateBinary (OPBINARY_SUB,
					CopyRecursive (psVar2->Vars.psBinary->psVar1),
					CreateInteger (psVar2->Vars.psBinary->psVar2->
					Vars.nInteger - psVar1->Vars.nInteger));
			}
			FreeRecursive (psOp);
		}
		else if ((psVar2->Vars.psBinary->eOpType == OPBINARY_DIVIDE)
			&& (psVar2->Vars.psBinary->psVar1->eOpType == OPTYPE_INTEGER)
			&& (psVar2->Vars.psBinary->psVar2->eOpType == OPTYPE_INTEGER)) {
			// Useful for simplifying continues fractions
			psReturn = CreateBinary (OPBINARY_DIVIDE, 
				CreateInteger (psVar2->Vars.psBinary->psVar1->Vars.nInteger
				+ (psVar2->Vars.psBinary->psVar2->Vars.nInteger * psVar1->Vars.nInteger)),
				CopyRecursive (psVar2->Vars.psBinary->psVar2));
			FreeRecursive (psOp);
		}
	}
	else if ((psVar1->eOpType == OPTYPE_BINARY)
		&& (psVar2->eOpType == OPTYPE_BINARY)
		&& (psVar1->Vars.psBinary->eOpType == OPBINARY_DIVIDE)
		&& (psVar2->Vars.psBinary->eOpType == OPBINARY_DIVIDE)
		&& (CompareOperations (psVar1->Vars.psBinary->psVar2, psVar2->Vars.psBinary->psVar2))) {
		// Paris of fractions
		psReturn = CreateBinary (OPBINARY_DIVIDE, CreateBinary (OPBINARY_ADD,
			CopyRecursive (psVar1->Vars.psBinary->psVar1),
			CopyRecursive (psVar2->Vars.psBinary->psVar1)), CopyRecursive (psVar1->Vars.psBinary->psVar2));
		FreeRecursive (psOp);
	}
	else if ((psVar2->eOpType == OPTYPE_UNARY)
		&& (psVar2->Vars.psUnary->eOpType == OPUNARY_NEGATIVE)) {
		// Change the addition of a negative in the second term into a subtraction
		psReturn = CreateBinary (OPBINARY_SUB,
			CopyRecursive (psVar1),
			CopyRecursive (psVar2->Vars.psUnary->psVar1));
		FreeRecursive (psOp);
	}
	else if ((psVar1->eOpType == OPTYPE_UNARY)
		&& (psVar1->Vars.psUnary->eOpType == OPUNARY_NEGATIVE)) {
		// Change the addition of a negative in the first term into a subtraction with
		// the terms switched around
		psReturn = CreateBinary (OPBINARY_SUB,
			CopyRecursive (psVar2->Vars.psUnary->psVar1),
			CopyRecursive (psVar1));
		FreeRecursive (psOp);
	}

	return psReturn;
}

/**
 * Recursively simplify a SUB operation if possible.
 * This is for internal use.
 *
 * @param psOp the operation to be simplified.
 * @param psVar1 the first parameter of the binary operation.
 * @param psVar2 the second parameter of the binary operation.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifySUB (Operation * psOp, Operation * psVar1, Operation * psVar2) {
	Operation * psReturn = psOp;

	if ((psVar1->eOpType == OPTYPE_INTEGER) && (psVar2->eOpType == OPTYPE_INTEGER)) {
		// Sutract two integers
		psReturn = CreateInteger (psVar1->Vars.nInteger - psVar2->Vars.nInteger);
		FreeRecursive (psOp);
	}
	else if ((psVar2->eOpType == OPTYPE_UNARY)
		&& (psVar2->Vars.psUnary->eOpType == OPUNARY_NEGATIVE)) {
		// Change the subtraction of a negitive in the second term into an addition
		psReturn = CreateBinary (OPBINARY_ADD,
			CopyRecursive (psVar1),
			CopyRecursive (psVar2->Vars.psUnary->psVar1));
		FreeRecursive (psOp);
	}

	return psReturn;
}

//////////////////////////////////////////////////////////////////
// Recursively simplify a MULTIPLY operation if possible
// This is for internal use
// psOp: the operation to be simplified
// psVar1: the first parameter of the binary operation
// psVar2: the second parameter of the binary operation
// returns: a simplifed version of the operation, which should be freed once no longer needed
Operation * SimplifyMUL (Operation * psOp, Operation * psVar1, Operation * psVar2) {
	Operation * psReturn = psOp;

	if ((psVar1->eOpType == OPTYPE_INTEGER) && (psVar2->eOpType == OPTYPE_INTEGER)) {
		// Multiply two integers
		psReturn = CreateInteger (psVar1->Vars.nInteger * psVar2->Vars.nInteger);
		FreeRecursive (psOp);
	}
	else if (psVar1->eOpType == OPTYPE_INTEGER) {
		if (psVar1->Vars.nInteger == 0) {
			// Multiply anything by 0 to get 0
			psReturn = CreateInteger (0);
			FreeRecursive (psOp);
		}
		else if (psVar1->Vars.nInteger == 1) {
			// Multiply anything by 1 to leave unchanged
			psReturn = CopyRecursive (psVar2);
			FreeRecursive (psOp);
		}
	}
	else if (psVar2->eOpType == OPTYPE_INTEGER) {
		if (psVar2->Vars.nInteger == 0) {
			// Multiply anything by 0 to get 0
			psReturn = CreateInteger (0);
			FreeRecursive (psOp);
		}
		else if (psVar2->Vars.nInteger == 1) {
			// Multiply anything by 1 to leave unchanged
			psReturn = CopyRecursive (psVar1);
			FreeRecursive (psOp);
		}
	}

	return psReturn;
}

/**
 * Recursively simplify a DIV operation if possible.
 * This is for internal use.
 *
 * @param psOp the operation to be simplified.
 * @param psVar1 the first parameter of the binary operation.
 * @param psVar2 the second parameter of the binary operation.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifyDIV (Operation * psOp, Operation * psVar1, Operation * psVar2) {
	Operation * psReturn = psOp;

	if ((psVar2->eOpType == OPTYPE_INTEGER) && (psVar2->Vars.nInteger == 1)) {
		// DIV by 1 leaves the operation unchanged
		psReturn = CopyRecursive (psVar1);
		FreeRecursive (psOp);
	}
	else if ((psVar1->eOpType == OPTYPE_INTEGER) && (psVar2->eOpType == OPTYPE_INTEGER)) {
		// DIV on two integers
		psReturn = CreateInteger (psVar1->Vars.nInteger / psVar2->Vars.nInteger);
		FreeRecursive (psOp);
	}

	return psReturn;
}

/**
 * Recursively simplify a DIVIDE operation if possible.
 * This is for internal use.
 *
 * @param psOp the operation to be simplified.
 * @param psVar1 the first parameter of the binary operation.
 * @param psVar2 the second parameter of the binary operation.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifyDIVIDE (Operation * psOp, Operation * psVar1, Operation * psVar2) {
	Operation * psReturn = psOp;

	if ((psVar2->eOpType == OPTYPE_INTEGER)
		&& (psVar2->Vars.nInteger == 1)) {
		// Divide by 1 leaves the operation unchanged
		psReturn = CopyRecursive (psVar1);
		FreeRecursive (psOp);
	}
	else if ((psVar1->eOpType == OPTYPE_INTEGER)
		&& (psVar2->eOpType == OPTYPE_INTEGER)) {
		// Reduce the division if possible
		int nGcd;
		nGcd = gcd (psVar1->Vars.nInteger, psVar2->Vars.nInteger);
		if (nGcd == psVar2->Vars.nInteger) {
			// The denominator divides the numerator precisely
			psReturn = CreateInteger (psVar1->Vars.nInteger
				/ psVar2->Vars.nInteger);
			FreeRecursive (psOp);
		}
		else {
			if (nGcd > 0) {
				// There's a GCD, so we can simplify both sides
				psVar1->Vars.nInteger /= nGcd;
				psVar2->Vars.nInteger /= nGcd;
			}
		}
	}
	else if ((psVar2->eOpType == OPTYPE_BINARY)
		&& (psVar2->Vars.psBinary->eOpType == OPBINARY_DIVIDE)) {
		// Division of a division can be inverted
		psReturn = CreateBinary (OPBINARY_DIVIDE, 
			CreateBinary (OPBINARY_MUL, CopyRecursive (psVar1), 
			CopyRecursive (psVar2->Vars.psBinary->psVar2)), 
			CopyRecursive (psVar2->Vars.psBinary->psVar1));
		FreeRecursive (psOp);
	}
	else if ((psVar1->eOpType == OPTYPE_BINARY)
		&& (psVar1->Vars.psBinary->eOpType == OPBINARY_DIVIDE)) {
		// Division of a division can be inverted
		psReturn = CreateBinary (OPBINARY_DIVIDE, 
			CopyRecursive (psVar1->Vars.psBinary->psVar1),
			CreateBinary (OPBINARY_MUL, CopyRecursive (psVar2), 
			CopyRecursive (psVar1->Vars.psBinary->psVar2))); 
		FreeRecursive (psOp);
	}

	return psReturn;
}

/**
 * Recursively simplify a POW operation if possible.
 * This is for internal use.
 *
 * @param psOp the operation to be simplified.
 * @param psVar1 the first parameter of the binary operation.
 * @param psVar2 the second parameter of the binary operation.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * SimplifyPOW (Operation * psOp, Operation * psVar1, Operation * psVar2) {
	Operation * psReturn = psOp;

	if (psVar2->eOpType == OPTYPE_INTEGER) {
		if (psVar2->Vars.nInteger == 0) {
			// Power by 0 always gives 1
			psReturn = CreateInteger (1);
			FreeRecursive (psOp);
		}
		else if (psVar2->Vars.nInteger == 1) {
			// Power by 1 leaves the operatoin unchanged
			psReturn = CopyRecursive (psVar1);
			FreeRecursive (psOp);
		}
		else if (psVar1->eOpType == OPTYPE_INTEGER) {
			if (psVar2->Vars.nInteger > 0) {
				// If they're both inetegers, and the exponent is positive, perform the calculation
				psReturn = CreateInteger ((int)pow ((double)psVar1->Vars.nInteger,
					(double)psVar2->Vars.nInteger));
				FreeRecursive (psOp);
			}
			else {
				// If the exponent is negative, turn it into a division
				psReturn = CreateBinary (OPBINARY_DIVIDE, CreateInteger (1), 
					CreateInteger ((int)pow ((double)psVar1->Vars.nInteger,
					(double)(-psVar2->Vars.nInteger))));
				FreeRecursive (psOp);
			}
		}
	}

	return psReturn;
}

/**
 * Continue simpliying an operation until it no longer changes. This 
 * repeadedly applies the proof rules until the Operation no longer changes.
 * Because a new version may created and the original may be freed
 * it's good practice to overwrite the variable pointing to the
 * input structure with the return value.
 * In general, it makes sense to use this rather than SimplifyOperation, since
 * the UberSimplifyOperation is more effective at simplifying an Operation.
 *
 * @param psOp the operation to simplify, which may be entirely freed.
 * @return a simplifed version of the operation, which should be freed once no 
 *         longer needed.
 *
 */
Operation * UberSimplifyOperation (Operation * psOp) {
	Operation * psPrev = NULL;

	// Repeatedly simplify
	do {
		FreeRecursive (psPrev);
		// Make a copy so we can compare the original with the result
		psPrev = CopyRecursive (psOp);
		psOp = SimplifyOperation (psOp);
		// Continue until the result no longer changes
	} while (!CompareOperations (psOp, psPrev));

	FreeRecursive (psPrev);

	return psOp;
}

