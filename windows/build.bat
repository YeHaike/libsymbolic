cd src

gcc -x c -I. -I.. -I../include -O2 -Wall -c Approximate.c Convert.c Differentiate.c Prop.c Simplify.c Variables.c -std=c99

ar rcs ../libsymbolic.a Approximate.o Convert.o Differentiate.o Prop.o Simplify.o Variables.o

gcc -x c -I. -I.. -I../include -O2 -Wall -o ../symbolic Interface.c -L.. -lstdc++ -lm -std=c99 -lsymbolic

cd ..
